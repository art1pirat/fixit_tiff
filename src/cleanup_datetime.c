/* fixes broken TIFF Files
 *
 * fixes invalid DateTime-field in Baseline-TIFFs,
 * based on http://www.awaresystems.be/imaging/tiff/tifftags/baseline.html
 *
 * author: Andreas Romeyke, 2013
 * licensed under conditions of libtiff
 */


#include "fixit_tiff.h"
#include "check_datetime.h"
#include "tiff_helper.h"

/** RULE0: default rule (string is correct) */
static int rule_default (const char * datestring, int * year, int * month, int * day, int * hour, int * min, int * sec) {
  if (FLAGGED == flag_be_verbose) printf ("rule00\n");
  if (6 == sscanf(datestring, "%04d:%02d:%02d%02d:%02d:%02d", year, month, day, hour, min, sec)) {
    return test_plausibility(year, month, day, hour, min, sec);
  } else {
    return -2;
  }
}

/** RULE1: fix: '18.03.2010 09:59:17' => '2010:03:18 09:59:17' */
static int rule_ddmmyyhhmmss_01 (const char * datestring, int * year, int * month, int * day, int * hour, int * min, int * sec) {
  if (FLAGGED == flag_be_verbose) printf ("rule01\n");
  if (6 == sscanf(datestring, "%02d.%02d.%04d%02d:%02d:%02d", day, month, year, hour, min, sec)) {
    return test_plausibility(year, month, day, hour, min, sec);
  } else {
    return -2;
  }
}

/** RULE2: fix: '2010-03-18 09:59:17' => '2010:03:18 09:59:17' */
static int rule_ddmmyyhhmmss_02 (const char * datestring, int * year, int * month, int * day, int * hour, int * min, int * sec) {
  if (FLAGGED == flag_be_verbose) printf ("rule02\n");
  if (6 == sscanf(datestring, "%04d-%02d-%02d%02d:%02d:%02d", year, month, day , hour, min, sec)) {
    return test_plausibility(year, month, day, hour, min, sec);
  } else {
    return -2;
  }
}

/** RULE3: fix 'Tue Dec 19 09:18:54 2006%0A' => '2006:12:19 09:18:45' */
static int rule_ddmmyyhhmmss_03 (const char * datestring, int * year, int * month, int * day, int * hour, int * min, int * sec) {
  if (FLAGGED == flag_be_verbose) printf ("rule03\n");
  char dow[4] = "\0\0\0\0";
  char monthstring[4] = "\0\0\0\0";
  static char* months[] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  };
  fprintf (stderr, "Datestring '%s'\n", datestring);
  int ret = sscanf(datestring, "%3s%3s%02d%02d:%02d:%02d%04d", dow, monthstring, day, hour, min, sec, year);
  if (7 == ret ) {
    *month=-1;
    for (int i = 0; i<= 11; i++) {
      if (strncmp (months[i], monthstring, 3) == 0) {
        *month=i+1;
        break;
      }
    }
    return test_plausibility(year, month, day, hour, min, sec);
  } else {
    return -2;
  }
}

/** RULENOFIX: dummy rule if no other rule matches, calls only exit */
static int rule_nofix (const char * datestring, int * year, int * month, int * day, int * hour, int * min, int * sec) {
  fprintf(stderr, "rule nofix, there is no applyable rule left, aborted without fixing problem\n");
  exit(FIXIT_TIFF_DATETIME_RULE_NOT_FOUND);
}

/** used for array of rules */
#define COUNT_OF_RULES 5
/** Array of rules */
int (*rules_ptr[COUNT_OF_RULES])(const char *, int *, int *, int *, int *, int *, int *) = {
  rule_default,
  rule_ddmmyyhhmmss_01,
  rule_ddmmyyhhmmss_02,
  rule_ddmmyyhhmmss_03,
  rule_nofix
};

/** corrects broken date string to expected format, see
 * http://www.awaresystems.be/imaging/tiff/tifftags/datetime.html
 * @param broken_datetime string with wrong datetime
 * @return string with corrected datetime
 */
char * correct_datestring (const char * broken_datetime) {
  int day;
  int month;
  int year;
  int hour;
  int min;
  int sec;
  /* if ret is wrong, you could try another rules to apply */
  int r;
  for (r = 0; r < COUNT_OF_RULES; r++) {
    if (FLAGGED == flag_be_verbose) printf("Applying rule%i", r);
    if (0 != (*rules_ptr[r])(broken_datetime, &year, &month, &day, &hour, &min, &sec)) {
      if (FLAGGED == flag_be_verbose) printf("applying next rule\n");
    } else {
      break;
    }
  }
  if (FLAGGED == flag_be_verbose) printf("datetime parsing of string '%s', year=%04d, month=%02d, day=%02d, hour=%02d, min=%02d, sec=%02d\n", broken_datetime, year, month, day, hour, min, sec);
  /* write corrected value to new string */
  char * fixed_date = malloc(sizeof(char) * TIFFDATETIMELENGTH); /* 20 comes from TIFF definition */
  if (NULL == fixed_date) {
    fprintf(stderr, "could not allocate memory for datetime conversion, abort\n");
    exit (FIXIT_TIFF_MEMORY_ALLOCATION_ERROR);
  }
  int written = snprintf(fixed_date, TIFFDATETIMELENGTH, "%04d:%02d:%02d %02d:%02d:%02d", year, month, day, hour, min, sec);

  if (written != (TIFFDATETIMELENGTH)-1) {
    fprintf(stderr, "something wrong, instead %d chars, %d chars were written\n",TIFFDATETIMELENGTH-1 ,written);
    exit (FIXIT_TIFF_STRING_COPY_ERROR);
  }
  return fixed_date;
}


/** loads a tiff, fix it if needed, stores tiff
 * @param filename filename which should be processed, repaired
 */
int cleanup_datetime(const char * filename ) {
  if (FIXIT_TIFF_IS_VALID == check_datetime(filename))  return FIXIT_TIFF_IS_VALID;
  else {
    /* load file */
    TIFF* tif = TIFFOpen(filename, "r+");
    if (NULL == tif) {
      fprintf( stderr, "file '%s' could not be opened\n", filename);
      exit (FIXIT_TIFF_READ_PERMISSION_ERROR);
    };
    /* find date-tag and fix it */
    char *datetime=NULL;
    uint32_t tag_counter=TIFFGetRawTagListCount(tif);
    uint32_t tagidx;
    for (tagidx=0; tagidx < tag_counter; tagidx++) {
      uint32_t tag = TIFFGetRawTagListEntry( tif, (int) tagidx );
      if (tag == TIFFTAG_DATETIME) {
        /*  via TIFFGetRawTagListEntry we have the tag
         *  read, the next 2 bytes are the type, next 2 count, last 4 value/offset */
        int fd = TIFFFileno( tif);
        uint16_t tagtype=0;
        if ( read( fd, &tagtype, 2) != 2 ) {
          perror ("TIFF Header read error tagtype");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        if (TIFFIsByteSwapped(tif))
          TIFFSwabShort(&tagtype);
        assert(TIFF_ASCII == tagtype);
	uint32_t bytepos_count = lseek( fd, 0, SEEK_CUR );
        uint32_t count=0;
        if ( read( fd, &count, 4) != 4 ) {
          perror ("TIFF Header read error tagcount");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        if (TIFFIsByteSwapped(tif))
          TIFFSwabLong(&count);
        uint32_t offset=0;
        if ( read( fd, &offset, 4) != 4 ) {
          perror ("TIFF Header read error offset");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        if (TIFFIsByteSwapped(tif))
          TIFFSwabLong(&offset);
        if (FLAGGED == flag_be_verbose) printf("tag=%u tagtype=%i c=%u offset=%u\n", tag, tagtype, count, offset);
	datetime = malloc( sizeof( char ) * count);
        if (NULL == datetime) {
          perror("Could not allocate memory for new datetime");
          exit(FIXIT_TIFF_MEMORY_ALLOCATION_ERROR);
        }
        /*  rewind to offset */
        if (lseek(fd, offset, SEEK_SET) != offset) {
          perror("TIFF datetime seek error to offset");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        if (read(fd, datetime, count) != count) {
          perror ("TIFF datetime read error offset");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        /* repair */
        char * new_datetime = correct_datestring( datetime );
        if (NULL == new_datetime) {
          perror("Could not allocate memory for new datetime");
          exit(FIXIT_TIFF_MEMORY_ALLOCATION_ERROR);
        }
        /*  rewind to offset */
        if (lseek(fd, offset, SEEK_SET) != offset) {
          perror("TIFF datetime seek error to offset");
          exit( FIXIT_TIFF_READ_ERROR );
        }
        /* write data back, only if no flag_check_only is set */
        if (write(fd, new_datetime, TIFFDATETIMELENGTH) != TIFFDATETIMELENGTH) {
          perror("TIFF datetime write error");
          exit(FIXIT_TIFF_WRITE_ERROR);
        }
	/*  rewind to IFD to set count */
        if (lseek(fd, bytepos_count, SEEK_SET) != bytepos_count) {
          perror("TIFF datetime seek error to IFD pos");
          exit( FIXIT_TIFF_READ_ERROR );
        }
	/* write corrected count, only if no flag_check_only is set */
	count = TIFFDATETIMELENGTH;
	if (TIFFIsByteSwapped(tif))
          TIFFSwabLong(&count);
        if (write(fd, &count, 4) != 4) {
          perror("TIFF datetime write error IFD count for datetime");
          exit(FIXIT_TIFF_WRITE_ERROR);
        }
	TIFFClose(tif);
	if (FLAGGED == flag_be_verbose) {
          printf("tag=%u tagtype=%i count=%u offset=%u (0x%04x)\n", tag, tagtype, count, offset, offset);
          printf("old_datetime='%s'\n", datetime);
          printf("new_datretime='%s'\n", new_datetime);
          printf("\n");
        }
	free(new_datetime);
	break;
      }
      if (FLAGGED == flag_be_verbose) printf("After  correction\n-----------------\n");
    }
  }
  if  (FIXIT_TIFF_IS_VALID == check_datetime (filename)) return FIXIT_TIFF_IS_CORRECTED;
  else return FIXIT_TIFF_IS_CHECKED;

}

