#ifndef FIXIT_TIFF_MD5_HELPER
#define FIXIT_TIFF_MD5_HELPER
#include <openssl/md5.h>
#include <unistd.h>
#include <stdio.h>
unsigned char * md5(unsigned long size, const unsigned char * buffer, unsigned char * digest);
void print_hexdigest(const unsigned char * digest);
#endif
/* FIXIT_TIFF_MD5_HELPER */
