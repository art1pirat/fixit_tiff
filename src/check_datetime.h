#ifndef FIXIT_TIFF_CHECK_DATETIME
#define FIXIT_TIFF_CHECK_DATETIME
#include <assert.h>
int check_datetime (const char *);
int test_plausibility (const int *year, const int *month, const int *day, const int *hour, const int *min, const int *sec);
#endif
/* FIXIT_TIFF_CHECK_DATETIME */
