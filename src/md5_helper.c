#include "md5_helper.h"

unsigned char * md5(unsigned long size, const unsigned char * iccdata, unsigned char * digest) {
    MD5_CTX md5_c;
    MD5_Init(&md5_c);
    MD5_Update(&md5_c, iccdata, size);
    MD5_Final(digest, &md5_c);
    return digest;
}

void print_hexdigest(const unsigned char * digest) {
    for (int i=0; i<16; i++) {
        printf("%02x ", digest[i]);
    }
    printf("\n");
}