#ifndef FIXIT_TIFF_TIFF_HELPER
#define FIXIT_TIFF_TIFF_HELPER
int TIFFGetRawTagListCount (TIFF * tif);
uint32_t TIFFGetRawTagListEntry( TIFF  * tif, int tagidx );
void print_baseline_tags (TIFF * tif);
void print_required_tags (TIFF * tif);
#endif
/* FIXIT_TIFF_TIFF_HELPER */
