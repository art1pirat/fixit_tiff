/* fixes broken TIFF Files
 *
 * fixes unused tags in Baseline-TIFFs,
 * based on http://www.awaresystems.be/imaging/tiff/tifftags/baseline.html
 *
 * author: Andreas Romeyke, 2013
 * licensed under conditions of libtiff
 */


#include "fixit_tiff.h"
#include "check_baseline.h"
#include "tiff_helper.h"
#include <tiffio.h>

/** load a tiff, clean it up if needed, store tiff
 * @param filename filename which should be processed, repaired
 */
int cleanup_baseline(const char * filename ) {
  if (
      (FIXIT_TIFF_IS_VALID == check_baseline (filename)) &&
      (FIXIT_TIFF_IS_VALID == check_required (filename))
     ) return FIXIT_TIFF_IS_VALID;
  else {
    /* load file */
    TIFF* tif = TIFFOpen(filename, "r+");
    if (NULL == tif) {
      fprintf( stderr, "file '%s' could not be opened\n", filename);
      exit (FIXIT_TIFF_READ_PERMISSION_ERROR);
    };
    uint32_t tag_counter=TIFFGetRawTagListCount(tif);
    uint32_t tagidx;
    uint32_t tags[tag_counter];
    for (tagidx=0; tagidx < tag_counter; tagidx++) {
      tags[tagidx] = TIFFGetRawTagListEntry( tif, (int) tagidx );
    }
    /* iterate through all tiff-tags in tiff file
     * delete all tags not in baselinetags
     */
    for (tagidx=0; tagidx < tag_counter; tagidx++) {
      printf ("found tag %u [0x%x] (%u)\n", tags[tagidx],tags[tagidx], tagidx);
      int found = 0;
      for (int baseline_index = 0; baseline_index < count_of_baselinetags; baseline_index++) {
        if (tags[tagidx] == baselinetags[baseline_index]) {
          printf ("DEBUG tag=%u base=%i idx=%i\n", tags[tagidx], baselinetags[baseline_index], baseline_index);
          found = 1;
          break;
        }
      }
      if (found == 0 ) {
        if (FLAGGED == flag_be_verbose) printf("removed tag %u\n", tags[tagidx]);
        TIFFUnsetField(tif, tags[tagidx]);
      }
    }
    if (FLAGGED == flag_be_verbose) printf("After  correction\n-----------------\n");
    if (FLAGGED == flag_be_verbose) TIFFPrintDirectory(tif, stdout, TIFFPRINT_NONE);
    /* write data back, only if no flag_check_only is set */
    int written = TIFFRewriteDirectory(tif); /* HINT: TIFFWriteDirectory is errorneous (because sometimes libtiff could extend IFD with missed tags automatically, use TIFFReWriteDirectory instead. This adds a new(!) IFD at the end of the TIFF file. */
    if (1 != written) {
      fprintf(stderr, "something is wrong, tiffdir could not be written to file '%s'\n", filename);
      exit (FIXIT_TIFF_WRITE_ERROR);
    }
    TIFFClose(tif);
  }
  if  (FIXIT_TIFF_IS_VALID == check_baseline (filename)) return FIXIT_TIFF_IS_CORRECTED;
  else return FIXIT_TIFF_IS_CHECKED;
}

