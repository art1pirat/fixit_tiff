#ifndef FIXIT_TIFF_CHECK_ICC_HEADER
#define FIXIT_TIFF_CHECK_ICC_HEADER
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

int parse_icc(unsigned long iccsize, char* iccdata, unsigned long errsize, char * errmessage);
int check_icc_header (const char * filename );
bool is_profileid_zero(unsigned long iccsize, const char* iccdata);
unsigned char * calc_profileid(unsigned long iccsize, const char* iccdata, unsigned char * digest);
bool check_profileid(unsigned long iccsize, const char* iccdata);
bool check_profilesize(unsigned long iccsize, const char* iccdata);
#endif
/* FIXIT_TIFF_CHECK_ICC_HEADER */